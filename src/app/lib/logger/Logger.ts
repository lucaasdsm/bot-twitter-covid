import { Format } from 'logform';
import winston, { createLogger, format, transports } from 'winston';
const { combine, timestamp, simple, colorize, printf } = format;

const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} - [${level}]: ${message}`;
});

const path = './logs';
const dateFormat = 'DD/MM/YYYY HH:mm:ss';

const customLevels = {
  levels: {
    info: 0,
    warn: 1,
    error: 2,
    debug: 3,
  },
  colors: {
    info: 'bold blue',
    warn: 'bold yellow',
    error: 'bold red',
    debug: 'bold white',
  },
};

winston.addColors(customLevels.colors);

const logFormat1: Format[] = [
  simple(),
  timestamp({ format: dateFormat }),
  myFormat,
];

const logFormat2: Format[] = [...logFormat1, colorize()];

class Logger {
  info(message?: string) {
    const logger = winston.createLogger({
      handleExceptions: true,
      format: combine(...logFormat2),
      transports: [
        new transports.Console({ format: combine(...logFormat1) }),
        new transports.File({ filename: `${path}/debug.log` }),
      ],
      exitOnError: false,
    });

    logger.info(message);
  }

  warn(message?: string) {
    const logger = createLogger({
      handleExceptions: true,
      format: combine(...logFormat2),
      transports: [
        new transports.Console({ format: combine(...logFormat1) }),
        new transports.File({ filename: `${path}/debug.log` }),
      ],
      exitOnError: false,
    });

    logger.warn(message);
  }

  error(message?: string) {
    const logger = createLogger({
      handleExceptions: true,
      format: combine(...logFormat2),
      transports: [new transports.Console({ format: combine(...logFormat1) })],
      exitOnError: false,
    });

    logger.error(message);
  }

  debug(message?: string) {
    const custom = 'debug';

    const logger = createLogger({
      levels: customLevels.levels,
      handleExceptions: true,
      format: combine(...logFormat2),
      transports: [
        new transports.Console({
          level: custom,
          format: combine(...logFormat1),
        }),
        new transports.File({
          level: custom,
          filename: `${path}/debug.log`,
        }),
      ],
      exitOnError: false,
    });

    logger.debug(message);
  }
}

export default new Logger();
