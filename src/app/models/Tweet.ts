import mongoose from '../api/mongo';

const TweetSchema = new mongoose.Schema({
  text: {
    type: String,
    require: true,
  },
  active: {
    type: Boolean,
    require: true,
    default: true,
  },
  date: {
    type: String,
    require: true,
    default: Date.now(),
  },
});

const Tweet = mongoose.model('Tweet', TweetSchema);

export default Tweet;
