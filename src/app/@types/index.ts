export const citiesList: CityType[] = [
  {
    name: 'Criciúma',
    value: 'CRICIUMA',
  },
  {
    name: 'Içara',
    value: 'ICARA',
  },
  {
    name: 'Imbituba',
    value: 'IMBITUBA',
  },
  {
    name: 'Laguna',
    value: 'LAGUNA',
  },
  {
    name: 'Tubarão',
    value: 'TUBARAO',
  },
  {
    name: 'Morro da Fumaça',
    value: 'MORRO DA FUMACA',
  },
];

export type CityType = {
  name: string;
  value: string;
};

// eslint-disable-next-line @typescript-eslint/no-inferrable-types
export const password: string = 'my_test_for_weak_security';
