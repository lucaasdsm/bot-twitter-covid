import mongoose from 'mongoose';

const {
  MONGO_DB_NAME,
  MONGO_DB_URL,
  MONGO_DB_USER,
  MONGO_DB_PASS,
} = process.env;

mongoose.connect(
  `mongodb+srv://${MONGO_DB_USER}:${MONGO_DB_PASS}@${MONGO_DB_URL}/${MONGO_DB_NAME}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  },
);

export default mongoose;
