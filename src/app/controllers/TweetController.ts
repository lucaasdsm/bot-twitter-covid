import { citiesList, CityType } from '../@types';
import covid from '../api/covid';
import twitter from '../api/twitter';
import Logger from '../lib/logger';

class TweetController {
  async run() {
    try {
      let tweet = 'Doses aplicadas até o momento:\n\n';

      for (const city of citiesList) {
        const filter = this._filterCity(city);

        const resCovid = await covid.post('/_count', filter);

        tweet += this._mountTweet(resCovid.data.count, city);
      }

      const resCovid = await covid.post('/_count');

      tweet += `\n\n`;
      tweet += this._mountTweet(resCovid.data.count);

      await twitter.post('statuses/update', {
        status: `${tweet}\n#covid19 #coronavirus #vacinas #dosesAplicadas`,
      });

      Logger.debug(tweet);
    } catch (e) {
      Logger.error(e.message);
    }
  }

  private _mountTweet(count: number, city?: CityType) {
    const total = count.toLocaleString().replace(/,/g, '.');
    return ` ${city ? city.name : 'Brasil'}: ${total}.\n`;
  }

  private _filterCity(city: CityType) {
    return {
      query: {
        bool: {
          must: [
            {
              match: {
                estabelecimento_municipio_nome: city.value,
              },
            },
            {
              match: {
                estabelecimento_uf: 'SC',
              },
            },
          ],
        },
      },
    };
  }

  // async create(req: Request, res: Response) {
  //   try {
  //     const { text }: { text: string } = req.body;
  //     if (!text && text.length < 5) const teste = await Tweet.create({ text });
  //     console.log('teste', teste);
  //     Logger.debug('Salvou');
  //   } catch (e) {
  //     console.error(e);
  //     Logger.error(e.message);
  //   }
  // }
}

export default new TweetController();
